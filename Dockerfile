FROM node:lts-alpine

RUN apk add --no-cache python3 pango cairo libxml2 libxslt bash curl jpeg \
        git build-base python3-dev py3-pip cairo-dev libxslt-dev libxml2-dev jpeg-dev

RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache-dir google-i18n-address pycountry intervaltree lxml requests six
RUN pip3 install --no-cache-dir 'pycairo>=1.18' 'weasyprint'
RUN ln -sf /usr/bin/python3 /usr/bin/python

RUN npm install -g pug-cli nodemon && npm cache clean --force

RUN apk del build-base python3-dev cairo-dev libxslt-dev libxml2-dev jpeg-dev

WORKDIR /root

ADD . /opt/xml2rfc

RUN mkdir -p /usr/share/fonts/custom
RUN ln -sf /opt/xml2rfc/cern/data/fonts /usr/share/fonts/custom

RUN cd /opt/xml2rfc && git rev-parse HEAD > .version
RUN rm -rf /opt/xml2rfc/.git

RUN apk del git

ENV PATH="/opt/nvm/bin:/opt/xml2rfc/cern:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
