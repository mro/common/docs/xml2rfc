
from xml2rfc.boilerplate_rfc_7841 import boilerplate_rfc_status_of_memo as rfc_status
from xml2rfc.strings import stream_name

stream_name['en-smm-apc'] = 'EN-SMM-APC'

rfc_status['en-smm-apc'] = {
    'std': {
        'true' : ["""<t>
        This is a CERN Standards Track document.
        </t>""",

        """<t>
        This document is a product of the CERN EN-SMM-APC team.  It represents
        the consensus of the CERN community.  It has
        received public review and has been approved for publication by
        the EN-SMM-APC section.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/std/by-id/{rfc_number}.html" />.
        </t>""",
        ],
        'false' : ["""<t>
        This is a CERN Standards Track document.
        </t>""",

        """<t>
        This document is a product of the CERN EN-SMM-APC team.  It has
        received public review and has been approved for publication by
        the EN-SMM-APC section.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/std/by-id/{rfc_number}.html" />.
        </t>""",
        ]
    },
    'bcp': {
        'true' : [ """<t>
        This memo documents a CERN Best Current Practice.
        </t>""",

        """<t>
        This document is a product of the CERN EN-SMM-APC team.  It represents
        the consensus of the CERN community.  It has
        received public review and has been approved for publication by
        the EN-SMM-APC section.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/bcp/by-id/{rfc_number}.html" />.
        </t>""",
        ],
        'false' : [ """<t>
        This memo documents a CERN Best Current Practice.
        </t>""",

        """<t>
        This document is a product of the CERN EN-SMM-APC team.  It has
        received public review and has been approved for publication by
        the EN-SMM-APC section.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/bcp/by-id/{rfc_number}.html" />.
        </t>""",
        ]
    },
    'exp': {
        'true' : [ """<t>
        This document is not a CERN EN-SMM-APC specification; it is
        published for examination, experimental implementation, and
        evaluation.
        </t>""",

        """<t>
        This document defines an Experimental Protocol for the CERN
        community.  This document is a product of the CERN EN-SMM-APC team.  It
        represents the consensus of the CERN community.  Not
        all documents approved by the EN-SMM-APC
        section are candidates for any level of Standard.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/exp/by-id/{rfc_number}.html" />.
        </t>""",
        ],
        'false': [ """<t>
        This document is not a CERN EN-SMM-APC specification; it is
        published for examination, experimental implementation, and
        evaluation.
        </t>""",

        """<t>
        This document defines an Experimental Protocol for the CERN
        community.  This document is a product of the CERN EN-SMM-APC team.  It
        has been approved for publication by the EN-SMM-APC section.  Not
        all documents approved by the EN-SMM-APC
        section are candidates for any level of Standard.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/exp/by-id/{rfc_number}.html" />.
        </t>""",
        ],
    },
    'historic': {
        'true' : [ """<t>
        This document is not a CERN EN-SMM-APC specification; it is
        published for the historical record.
        </t>""",

        """<t>
        This document defines a Historic Document for the CERN
        community.  This document is a product of the CERN EN-SMM-APC team.  It
        represents the consensus of the CERN community.
        It has received public review and has been approved for publication by
        the EN-SMM-APC section.  Not all documents approved by the EN-SMM-APC
        section are candidates for any level of Standard.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/historic/by-id/{rfc_number}.html" />.
        </t>""",
        ],
        'false': [ """<t>
        This document is not a CERN EN-SMM-APC specification; it is
        published for the historical record.
        </t>""",

        """<t>
        This document defines a Historic Document for the CERN
        community.  This document is a product of the CERN EN-SMM-APC team.  It
        has been approved for publication by the EN-SMM-APC section.  Not all
        documents approved by the EN-SMM-APC section are candidates for any
        level of Standard.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/historic/by-id/{rfc_number}.html" />.
        </t>""",
        ],
    },
    'info': {
        'true' : [ """<t>
        This document is not a CERN EN-SMM-APC specification; it is
        published for informational purposes.
        </t>""",

        """<t>
        This document is a product of the CERN EN-SMM-APC team.  It represents
        the consensus of the CERN community.  It has
        received public review and has been approved for publication by the
        EN-SMM-APC section.  Not all documents approved by the EN-SMM-APC
        section are candidates for any level of Standard.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/info/by-id/{rfc_number}.html" />.
        </t>""",
        ],
        'false': [ """<t>
        This document is not a CERN EN-SMM-APC specification; it is
        published for informational purposes.
        </t>""",

        """<t>
        This document is a product of the CERN EN-SMM-APC team.  It has been
        approved for publication by the EN-SMM-APC section.  Not all documents
        approved by the EN-SMM-APC section are candidates for any level of Standard.
        </t>""",

        """<t>
        Information about the current status of this document, any
        errata, and how to provide feedback on it may be obtained at
        <eref target="{scheme}://www.cern.ch/apc-dev/docs/info/by-id/{rfc_number}.html" />.
        </t>""",
        ],
    },
}
