
from os import path

from xml2rfc.boilerplate_tlp import boilerplate_tlp as tlp
from xml2rfc.boilerplate_id_guidelines import boilerplate_draft_status_of_memo as draft_status
from xml2rfc.writers.preptool import PrepToolWriter
from xml2rfc.writers.paginated_txt import PaginatedTextRfcWriter
from xml2rfc.writers.text import TextWriter
from xml2rfc.writers.base import BaseV3Writer
from xml2rfc.writers.html import HtmlWriter
from xml2rfc.utils import build_dataurl

script_dir = path.dirname(path.realpath(__file__))

tlp['5.0']['alt'] = [
    """<t>
    Copyright (c) {year}, CERN. All rights reserved.
    All reproduction, use or distribution of this document, in whole or
    in part, by any means, without CERN prior written approval, is
    strictly forbidden.
    </t>"""
]

draft_status[0] = """<t>
    This document is a CERN Draft.
    </t>"""
draft_status[1] = """<t>
    CERN Drafts are working documents of the CERN community. Note that other
    groups may also distribute working documents as Drafts.
    </t>"""
draft_status[2] = """<t>
    CERN Drafts are draft documents valid for a maximum of six months
    and may be updated, replaced, or obsoleted by other documents at any
    time. It is inappropriate to use Drafts as reference
    material or to cite them other than as "work in progress."
    </t>"""
draft_status[3] = """<t>
    This Draft will expire on {expiration_date}.
    </t>"""

def front_insert_series_info(self, e, p):
    if self.options.rfc:
        edms = e.find('./seriesInfo[@name="EDMS"]')
        if edms is None:
            title = e.find('title')
            if title != None:
                pos = e.index(title)+1
            else:
                pos = 0
            e.insert(pos, self.element('seriesInfo', name='EDMS', value=self.rfcnumber))
        elif edms.get('value') != self.rfcnumber:
            self.die(e, "Expected a <seriesInfo name=\"EDMS\"> to match rfc number")
PrepToolWriter.front_insert_series_info = front_insert_series_info

def wrap_paginated(fun):
    def wrapper(self):
        fun(self)
        self.left_header = 'Draft'
    return wrapper
PaginatedTextRfcWriter.pre_rendering = wrap_paginated(PaginatedTextRfcWriter.pre_rendering)

def wrap_first_page(fun):
    def wrapper(self, e, width, **kwargs):
        ret = fun(self, e, width, **kwargs)
        ret = ret.replace('Internet-Draft', 'Draft')
        ret = ret.replace('Network Working Group', 'CERN Community')
        return ret
    return wrapper
TextWriter.render_first_page_top = wrap_first_page(TextWriter.render_first_page_top)

def wrap_page_top_left(fun):
    def wrapper(self, *args, **kwargs):
        if self.root.get('ipr') == 'none':
            return ''
        number = self.root.get('number')
        if number != None:
            return 'EDMS %s' % number
        info = self.root.find('./front/seriesInfo[@name="EDMS"]')
        if info != None:
            return 'EDMS %s' % info.get('value')
        return 'Draft'
    return wrapper
BaseV3Writer.page_top_left = wrap_page_top_left(BaseV3Writer.page_top_left)

category_tag = {
    'rfc': { 'tag': 'RFC', 'color': '#0073A4' },
    'std': { 'tag': 'STD', 'color': '#0073A4' },
    'bcp': { 'tag': 'BCP', 'color': '#9FC204' },
    'exp': { 'tag': 'EXP', 'color': '#E3006A' },
    'info': { 'tag': 'INFO', 'color': '#fad000' },
    'historic': { 'tag': 'Historic', 'color': '#485c74' },
    'draft': { 'tag': 'Draft', 'color': '#868686'}
}
def wrap_render_rfc(fun):
    def wrapper(self, *args, **kwargs):
        html = fun(self, *args, **kwargs)
        title = html.find('head/title')
        if title is not None and title.text.startswith('RFC'):
            title.text = title.text.replace('RFC', 'EDMS', 1)

        issn = html.find('body/div/dl/dt[@class="label-issn"]')
        if issn is not None:
            issn.getparent().remove(issn)

        selector = 'body/div[@class="document-information"]/dl/dd[@class="%s"]'
        if self.options.rfc:
            category = self.root.get('category')
            cat_info = html.find(selector % 'category')
        else:
            category ='draft'
            cat_info = html.find(selector % 'intended-status')
        if category in category_tag and cat_info is not None:
            cat_info.set('data-tag', category_tag[category]['tag'])
            style = html.makeelement('style')
            style.set('type', 'text/css')
            style.text = '''
                @page :first {
                    @top-right { color: %s; }
                }''' % category_tag[category]['color']

            html.append(style)

        # Override header-icon depending on submissionType
        stream = self.root.get('submissionType')
        try:
            with open('%s/data/%s.svg' % (script_dir, stream)) as f:
                data = f.read()
                style = html.makeelement('style')
                style.set('type', 'text/css')
                style.text = '''
                    @page :first {
                        @top-center { content: url(%s) !important; }
                    }''' % (build_dataurl('image/svg+xml', data))
                html.find('head').append(style)
        except:
            pass

        return html
    return wrapper
HtmlWriter.render_rfc = wrap_render_rfc(HtmlWriter.render_rfc)
