#!/bin/sh

SCRIPT_DIR=$(cd $(dirname $0); pwd)

docker build -f "${SCRIPT_DIR}/Dockerfile" -t cern/cc7/rfcgen .
