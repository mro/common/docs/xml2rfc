#!/bin/sh

# Very simple sh based test

TEST_DIR=$(cd $(dirname $0); pwd)

set -e

cd ${TEST_DIR}

function die() {
    echo "Error: $1" >&2
    exit 1
}

function not_rfc_test() {
    grep -q 'Internet-Draft' "$1" && die 'Should not be an Internet-Draft' || true
    grep -q 'IETF' "$1" && die 'Should not be an IETF document' || true
}

function gen_template_doc() {
    rm -rf "$1."{xml,txt,pdf,html}
    xml2rfc "$1.pug"
    xml2rfc --reference "$1.pug"
}

gen_template_doc draft-template
not_rfc_test draft-template.txt
not_rfc_test draft-template.html
grep -q 'CERN Draft' draft-template.txt || die 'Should be a CERN Draft'

gen_template_doc std-template
not_rfc_test std-template.txt
not_rfc_test std-template.html
grep -q 'CERN Standards Track' std-template.txt || die 'Should be a CERN Standards Track'

gen_template_doc info-template
gen_template_doc bcp-template
gen_template_doc historic-template
gen_template_doc exp-template

xml2rfc info-template.pug --is-expired && die 'Should never expire (not a draft)' || true
xml2rfc expired-draft.pug --is-expired || die 'Should be expired'

xml2rfc info-template.pug --is-draft && die 'Should not be a draft' || true
xml2rfc expired-draft.pug --is-draft || die 'Should be a draft'

gen_template_doc artwork-template

echo 'done'
