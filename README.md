# xml2rfc writer

RFC like document generator, generating txt/HTML/PDF documents.

Forked from [xml2rfc](https://trac.tools.ietf.org/tools/xml2rfc/trac/)

## Install

To use this tool, the easiest way is to use the prebuilt docker image:
```bash
docker pull gitlab-registry.cern.ch/mro/common/docs/xml2rfc

# Will bind current directory to /root in docker container
docker run --rm -it -v $PWD:/root gitlab-registry.cern.ch/mro/common/docs/xml2rfc sh
```

To list required dependencies and install this tool on your own, please refer to [Dockerfile](Dockerfile)

## Usage

In the docker container:
```bash
# To generate pdf,txt,xml
xml2rfc draft-document.xml

# This tool also supports [pug](https://pugjs.org) formatted files:
xml2rfc draft-document.pug

# And can watch a file for modifications
xml2rfc-watch draft-document.pug
```

It is recommended to add short-hand for those commands in your *.bashrc*:
```bash
xml2rfc-watch() {
  docker run --rm -it -v $PWD:/root gitlab-registry.cern.ch/mro/common/docs/xml2rfc xml2rfc-watch "$@"
}

xml2rfc() {
  docker run --rm -it -v $PWD:/root gitlab-registry.cern.ch/mro/common/docs/xml2rfc xml2rfc "$@"
}
```

### WaveDrom support

To quickly inject [WaveDrom](https://wavedrom.com/) diagrams:
1. Edit your diagram on [WaveDrom editor](https://wavedrom.com/editor.html)
2. Click on Expand URL from the menu
3. Copy the content displayed after `https://wavedrom.com/editor.html?` in your browser's url
4. Paste it in your document, prefixing with `https://svg.wavedrom.com/` as follows:
```pug
artwork(type="binary-art" align="center" src='https://svg.wavedrom.com/%7Bsignal%3A%20%5B%0A%20%20%7Bname%3A%20%27clk%27%2C%20wave%3A%20%27p.....%7C...%27%7D%2C%0A%20%20%7Bname%3A%20%27dat%27%2C%20wave%3A%20%27x.345x%7C%3D.x%27%2C%20data%3A%20%5B%27head%27%2C%20%27body%27%2C%20%27tail%27%2C%20%27data%27%5D%7D%2C%0A%20%20%7Bname%3A%20%27req%27%2C%20wave%3A%20%270.1..0%7C1.0%27%7D%2C%0A%20%20%7B%7D%2C%0A%20%20%7Bname%3A%20%27ack%27%2C%20wave%3A%20%271.....%7C01.%27%7D%0A%5D%7D%0A')
```

To modify an existing diagram, simply copy back the url after `https://svg.wavedrom.com/`, prefixing it with `https://wavedrom.com/editor.html?` in your browser.
